import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PlansService } from './../../../../core/services/plans.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-my-plan',
  templateUrl: './my-plan.component.html',
  styleUrls: ['./my-plan.component.css']
})
export class MyPlanComponent implements OnInit {

  userId = localStorage.getItem('id') || sessionStorage.getItem('id');
  planId;
  payment;
  constructor(
    private plansService: PlansService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getPlan(this.userId);
  }
  
  getPlan(userId) {
    this.spinner.show();
    this.plansService.getPlan(userId).subscribe(response => {
      this.spinner.hide();
      if (response) {
        this.planId = response.contract.plan_id;
      }else{
        this.getPlanByBoleto(this.userId);
      }
    })
  }

  getPlanByBoleto(userId){
    this.spinner.show();
    this.plansService.getPlanByBoleto(userId).subscribe(response => {
      this.spinner.hide();
      this.planId = response.plan.id;
      this.payment = response.payment;
    })
  }

  removePlan() {
    Swal.fire({
      title: 'Tem certeza que deseja cancelar seu plano?',
      text: "Essa ação não poderá ser revertida.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();
        this.plansService.cancelPlan(this.userId, this.planId).subscribe(response => {
          this.spinner.hide();
          Swal.fire(
            '',
            'A solicitação de cancelamento do plano foi feita com sucesso, a Rebox entrará em contato em breve.',
            'success'
          );
        }, error => {
          this.spinner.hide();
          Swal.fire({
            titleText: 'Opss, sinto muito :(',
            text: `${error.error.message || 'Houve um erro ao tentar efetuar o cancelamento. Caso o problema persista, solicite enviando um e-mail para contato@rebox.com.br'}`,
            icon: 'error'
          })
        })
      }
    })
  }

}
