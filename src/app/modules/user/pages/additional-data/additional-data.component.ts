import { UserService } from './../../../../core/services/user.service';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-additional-data',
  templateUrl: './additional-data.component.html',
  styleUrls: ['./additional-data.component.css']
})
export class AdditionalDataComponent implements OnInit {

  plan;
  id = localStorage.getItem('id');

  additionalForm = this.fb.group({
    model: ['', Validators.required],
    brand: ['', Validators.required],
    board: ['', Validators.required],
    motor: ['', Validators.required],
    year: ['', Validators.required],
    oil_changes: ['', Validators.required]
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private userService: UserService
  ) {
    this.route.queryParams.subscribe(response => {
      this.plan = response.plan;
    });
  }

  ngOnInit(): void {
    this.getUserVehicle(this.id)
  }

  getUserVehicle(id) {
    this.userService.getUser(id).subscribe(response => {
      console.log(response)
      this.additionalForm.patchValue(response.vehicles[0]);
    })
  }

  checkOilChanges(value) {
    if (value >= 12) {
      return this.additionalForm.get('oil_changes').patchValue(12);
    }
    else if(value < 0) {
      return this.additionalForm.get('oil_changes').patchValue(1);
    }

    return this.additionalForm.get('oil_changes').patchValue(value);
  }

  openModal() {
    if (this.additionalForm.valid) {
      return ($('#modalExemplo') as any).modal('show');
    }
    return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning');
  }

  nextPage() {
    if (this.additionalForm.valid) {
      return this.router.navigate(['/user/payment'], {
        queryParams: {
          plan: this.plan
        }
      });
    }
  }

}
