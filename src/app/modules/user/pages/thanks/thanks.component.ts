import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { PlansService } from './../../../../core/services/plans.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-thanks',
  templateUrl: './thanks.component.html',
  styleUrls: ['./thanks.component.css']
})
export class ThanksComponent implements OnInit {

  userId = localStorage.getItem('id') || sessionStorage.getItem('id');
  planId;
  payment_method;
  methodId;
  constructor(
    private plansService: PlansService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route
      .queryParams
      .subscribe(params => {
        this.planId = params.p;
        this.methodId = params.m;
        this.payment_method = params.m === 'b' 
        ? 'por boleto' : params.m === 'c'
        ? 'por cartão de crédito' : params.m === 'd'
        ? 'por cartão de débito' : null;
      })
  }

  linkHome(){
    this.router.navigate(['/'])
  }

}
