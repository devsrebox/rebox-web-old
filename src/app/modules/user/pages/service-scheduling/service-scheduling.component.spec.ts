import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceSchedulingComponent } from './service-scheduling.component';

describe('ServiceSchedulingComponent', () => {
  let component: ServiceSchedulingComponent;
  let fixture: ComponentFixture<ServiceSchedulingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceSchedulingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceSchedulingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
