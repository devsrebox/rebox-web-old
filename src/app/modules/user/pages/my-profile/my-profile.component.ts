import { FormBuilder, Validators } from '@angular/forms';
import { UtilsService } from './../../../../core/services/utils.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from './../../../../core/services/user.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { CpfCnpjValidator } from 'ngx-cpf-cnpj';
@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  imgPost;
  imgRead;
  imgUrlForm;

  userId = localStorage.getItem('id') || sessionStorage.getItem('id');

  userForm = this.fb.group({
    name: ['', Validators.required],
    cpf: ['', [Validators.required, CpfCnpjValidator.validate]],
    email: ['', [Validators.required, Validators.email]],
    cel: ['', Validators.required],
    vehicles: []
  });

  constructor(
    private userService: UserService,
    private spinner: NgxSpinnerService,
    private utilsService: UtilsService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getUser(this.userId);
  }

  getUser(id) {
    this.spinner.show();
    this.userService.getUser(id).subscribe(response => {
      this.spinner.hide();
      this.userForm.patchValue(response);
      this.imgUrlForm = response.img_profile;
    })
  }

  sendImg(event): void {
    this.spinner.show();
    this.imgPost = new FormData();

    const reader = new FileReader();
    this.imgRead = true;

    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      this.imgUrlForm = (reader.result as string);
    };
    this.imgPost.append('profile', event.target.files[0]);
    this.userService.updateImage(this.imgPost).subscribe(response => {
      this.spinner.hide();
      Swal.fire({
        icon: 'success',
        text: 'Imagem de perfil atualizada com sucesso!'
      });
      localStorage.removeItem('image_url');
      localStorage.setItem('image_url', response.data.img_profile);
      this.utilsService.headerImg.next(response.data.img_profile);
    }, error => {
      this.spinner.hide();
    }, () => {

    });
  }

  onSubmit() {
    if (this.userForm.invalid) {
      return Swal.fire({
        icon: 'warning',
        text: 'Verifique se os dados foram inseridos corretamente!'
      });
    }
    this.spinner.show();
    return this.userService.updateUser(this.userId, this.userForm.value).subscribe(response => {
      this.spinner.hide();
      Swal.fire({
        title: 'Atenção',
        icon: 'success',
        text: 'Usuário atualizado com sucesso!'
      });
    }, error => {
      this.spinner.hide();
    });
  }

}
