import { NgxMaskModule } from 'ngx-mask';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { CompaniesRoutingModule } from './companies-routing.module';
import { SharedModule } from '../../shared/shared.module';

import { SigninComponent } from './pages/signin/signin.component';
import { SignupComponent } from './pages/signup/signup.component';
import { UtilitiesModule } from '../../utilities/utilities.module';
import { RequestsComponent } from './pages/requests/requests.component';
import { RequestDetailComponent } from './pages/request-detail/request-detail.component';
import { MyProfileComponent } from './pages/my-profile/my-profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { RegisterEmployeeComponent } from './pages/register-employee/register-employee.component';
import { EmployeesComponent } from './pages/employees/employees.component';
import { FinancialReportComponent } from './pages/financial-report/financial-report.component';
import { OperationalReportComponent } from './pages/operational-report/operational-report.component';
import { ServiceDetailComponent } from './pages/service-detail/service-detail.component';


@NgModule({
  declarations: [
    SigninComponent,
    SignupComponent,
    RequestsComponent,
    RequestDetailComponent,
    MyProfileComponent,
    DashboardComponent,
    ReportsComponent,
    RegisterEmployeeComponent,
    EmployeesComponent,
    FinancialReportComponent,
    OperationalReportComponent,
    ServiceDetailComponent
  ],
  imports: [
  CommonModule,
    CompaniesRoutingModule,
    SharedModule,
    UtilitiesModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule
  ]
})
export class CompaniesModule { }
