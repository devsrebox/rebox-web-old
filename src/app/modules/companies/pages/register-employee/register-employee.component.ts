import { NgxSpinnerService } from 'ngx-spinner';
import { EmployeeService } from './../../../../core/services/employee.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { CpfCnpjValidator } from 'ngx-cpf-cnpj';

@Component({
  selector: 'app-register-employee',
  templateUrl: './register-employee.component.html',
  styleUrls: ['./register-employee.component.css']
})
export class RegisterEmployeeComponent implements OnInit {
  imgUrlForm;
  imgPost;
  imgRead;

  url: any;
  isEdit = false;
  partnerId = localStorage.getItem('c_id');
  employeeId;

  employeeForm = this.fb.group({
    first_name: ['', Validators.required],
    last_name: ['', Validators.required],
    name: [''],
    // rg: ['', Validators.required],
    cpf: ['', [Validators.required, CpfCnpjValidator]],
    // cep: [''],
    // address: [''],
    // number: [''],
    // neighborhood: [''],
    // city: [''],
    // uf: [''],
    cel: ['', Validators.required],
    email: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(6)]],
    confirm_password: [''],
    role: ['partner']
  });

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService
  ) {
    this.url = this.router.url.split('/');
    this.route.params.subscribe(response => {
      this.employeeId = response.id;
    });
  }

  ngOnInit(): void {
    if (this.url[2] === 'edit-employee') {
      this.isEdit = true;
      this.getEmployee(this.partnerId, this.employeeId);
    }
  }

  getEmployee(partnerId, id): void {
    this.spinner.show();
    this.employeeService.getEmployeeById(partnerId, id).subscribe(response => {
      const first_name = response.user.name.split(' ')[0];
      const last_name = response.user.name.split(' ')[1];
      this.employeeForm.get('first_name').patchValue(first_name);
      this.employeeForm.get('last_name').patchValue(last_name);
      this.employeeForm.get('confirm_password').patchValue(response.user.password);
      this.employeeForm.patchValue(response.user);
      this.imgUrlForm = response.user.img_profile;
      this.spinner.hide();
    });
  }

  sendImg(event): void {
    this.imgPost = new FormData();

    const reader = new FileReader();

    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      this.imgUrlForm = (reader.result as string);
    };
    this.imgPost.append('profile', event.target.files[0]);
  }

  removeEmployee() {
    return Swal.fire({
      title: 'Você tem certeza?',
      text: 'Após confirmar essa ação não poderá ser revertida!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Remover'
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();
        this.employeeService.removeEmployee(this.partnerId, this.employeeId).subscribe(response => {
          this.spinner.hide();
          Swal.fire(
            'Removido!',
            'Atendente removido com sucesso.',
            'success'
          );
          this.router.navigate(['/companies/employees']);
        });
      }
    });

  }


  onSubmit() {
    const name = `${this.employeeForm.get('first_name').value} ${this.employeeForm.get('last_name').value}`;
    this.employeeForm.get('name').patchValue(name);
    if (this.employeeForm.invalid) {
      return Swal.fire('Campos inválidos', 'Insira os dados corretamente.', 'warning');
    }

    if(!this.isEdit) {
      this.spinner.show();
      return this.employeeService.createEmployee(this.partnerId, this.employeeForm.value).subscribe(response => {
        this.spinner.hide();
        Swal.fire('Cadastrado', 'Atendente cadastrado com sucesso!', 'success');
        return this.router.navigate(['/companies/employees']);
      }, error => {
        this.spinner.hide();
        Swal.fire('Erro', error.error.message, 'error');
      });
    } else {
      this.spinner.show();
      return this.employeeService.updateEmployee(this.partnerId, this.employeeId ,this.employeeForm.value).subscribe(response => {
        this.spinner.hide();
        Swal.fire('Atualizado', 'Atendente atualizado com sucesso!', 'success');
        return this.router.navigate(['/companies/employees']);
      }, error => {
        this.spinner.hide();
        Swal.fire('Erro', error.error.message, 'error');
      });
    }

  }

}
