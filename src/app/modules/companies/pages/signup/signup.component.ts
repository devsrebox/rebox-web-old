import { UtilsService } from './../../../../core/services/utils.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray } from '@angular/forms';

import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { AuthService } from '../../../../core/auth/auth.service';
import { CnpjValidator } from '../../../../shared/validators/cnpj.validator';
import { ThrowStmt } from '@angular/compiler';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  isSubmited = false;

  states;
  cities;
  stateName;
  cityName;

  regionState: any ;
  regionCity = [];

  regions = [];
  clicked = false;

  types = [];

  signUpForm = this.fb.group({
    cod: [''],
    name: ['', Validators.required],
    cnpj: ['', [Validators.required, CnpjValidator]],
    email: ['', Validators.required],
    cel: ['', Validators.required],
    location: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(6)]],
    town: [''],
    oil_change: [''],
    others: [''],
    types: [[], [Validators.required]],
    role: ['partner'],
    serviceLocations: [[]]
  });

  regionsForm = this.fb.group({
    state: ['', [Validators.required]],
    city: ['', [Validators.required]],
    moreRegions: this.fb.array([this.createFormGroup()])
  });


  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private utilsService: UtilsService
  ) { }

  ngOnInit(): void {
    this.moreRegions.removeAt(0);
    this.getStates();
    this.generateCode();
  }

  generateCode() {
    const hexa = '0123456789ABCDEF';
    let code = '';

    for (let i = 0; i < 6; i++) {
      code += hexa[Math.floor(Math.random() * 16)];
    }
    this.signUpForm.get('cod').patchValue(code);
  }

  createFormGroup() {
    return this.fb.group({
      state: ['', Validators.required],
      city: ['', Validators.required],
    });
  }

  get moreRegions(): FormArray {
    return this.regionsForm.get('moreRegions') as FormArray;
  }

  addOtherRegion(): void {
    if (( this.moreRegions as FormArray).length < 9) {
      ( this.moreRegions as FormArray).push(this.createFormGroup());
      this.clicked = true;
    }
  }

  removeRegion(index): void {
    this.moreRegions.removeAt(index);
  }

  getStates() {
    this.spinner.show();
    this.utilsService.getStates().subscribe(response => {
      this.states = response.map((uf) => {
        const state = {
          id: uf.id,
          sigla: uf.sigla,
          nome: uf.nome
        };
        return state;
      }).sort((a, b) => {
        const x = a.nome.toLowerCase();
        const y = b.nome.toLowerCase();
        if (x < y) {
          return -1;
        }
        if (x > y) {
          return 1;
        }
      });
      this.regionState = response;
      this.spinner.hide();
    });
  }

  onStateChange(event) {
    this.stateName = event;
    let idState;
    for (const item of this.states) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityById(idState);
        break;
      }
    }
  }

  onStateRegionChange(event) {
    let idState;
    for (const item of this.regionState) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityRegionById(idState);
        break;
      }
    }
  }

  getCityRegionById(id) {
    this.utilsService.getCity(id).subscribe(response => {
      this.regionCity.push(response);
    });
  }

  getCityById(id) {
    this.utilsService.getCity(id).subscribe(response => {
      this.cities = response;
    });
  }

  onCityChange(event) {
    this.cityName = event;
  }

  setRegions() {
    const regions = [
      {
        state: this.regionsForm.get('state').value,
        city: this.regionsForm.get('city').value,
      },
      ...this.regionsForm.get('moreRegions').value
  ];
    this.signUpForm.get('serviceLocations').patchValue(regions);
    this.onSubmit();
  }

  onSubmit() {
    this.signUpForm.controls.types.patchValue(this.types);
    this.isSubmited = true;

    if (this.signUpForm.invalid) {
      return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning');
    }

    this.spinner.show();
    this.authService.signup(this.signUpForm.value).subscribe(
      response => {
        // TODO - PERSONALIZAR CSS
        Swal.fire({
          title: 'Cadastro enviado com sucesso',
          text: 'Em breve entraremos em contato com você.',
          confirmButtonText: 'Voltar'
        }).then(result => {
          this.router.navigate(['/companies/signin']);
        });
        this.spinner.hide();
      },
      error => {
        Swal.fire(
          'Oops...',
          `${error.error.message}`,
          'error'
        );
        this.spinner.hide();
      }
    );
  }

  changeTypeService(event) {
    const index = this.types.indexOf(parseInt(event))
    if (index > -1) {
      this.types.splice(index, 1);
    } else {
      this.types.push(parseInt(event))
    }
  }

}
