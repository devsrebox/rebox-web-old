import { UtilsService } from './../../../../core/services/utils.service';
import { UserService } from './../../../../core/services/user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { PartnersService } from './../../../../core/services/partners.service';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { CnpjValidator } from 'src/app/shared/validators/cnpj.validator';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css'],
})
export class MyProfileComponent implements OnInit {
  userId = localStorage.getItem('c_id');
  isSubmitted = false;

  imgPost;
  imgRead;
  imgUrlForm;
  types = [];

  states;
  cities;
  stateName;
  cityName;

  regionState: any;
  regionCity = [];

  regions = [];
  clicked = false;

  profileForm = this.fb.group({
    contact1: ['', Validators.required],
    contact2: [''],
    number_trucks: ['', Validators.required],
    cod: ['', Validators.required],
    cnpj: ['', [Validators.required, CnpjValidator]],
    company_name: ['', Validators.required],
    name: ['', Validators.required],
    ie: ['', Validators.required],
    im: ['', Validators.required],
    cep: ['', Validators.required],
    address: ['', Validators.required],
    city: ['', Validators.required],
    uf: ['', Validators.required],
    neighborhood: ['', Validators.required],
    number: ['', Validators.required],
    contract_of: [''],
    contract_until: [''],
    email: ['', Validators.required],
    site: [''],
    types: ['', Validators.required],
    tow: [''],
    changeOil: [''],
    others: [''],
    user_id: ['', Validators.required],
    serviceLocations: [''],
    bank_account: this.fb.group({
      bank_number: [''],
      account_agency: [''],
      account_current: [''],
    }),
    user: this.fb.group({
      password: [''],
      confirm_password: [''],
    }),
  });

  regionsForm = this.fb.group({
    moreRegions: this.fb.array([this.createFormGroup()]),
    state: [''],
    city: [''],
  });

  constructor(
    private fb: FormBuilder,
    private partnerService: PartnersService,
    private spinner: NgxSpinnerService,
    private userService: UserService,
    private utilsService: UtilsService
  ) {}

  ngOnInit(): void {
    this.getPartner(this.userId);
    this.moreRegions.removeAt(0);
  }

  getPartner(id) {
    let serviceLocations;
    this.spinner.show();
    this.partnerService.getPartner(id).subscribe(
      (response) => {
        if (!response.bank_account) {
          response.bank_account = {
            bank_number: '',
            account_agency: '',
            account_current: '',
          };
        }
        console.log(response);
        this.profileForm.patchValue(response);
        this.imgUrlForm = response.user.img_profile;

        if (response.serviceLocations) {
          serviceLocations = response.serviceLocations.slice(1);
          this.regionsForm.patchValue(response.serviceLocations[0]);
        }
        this.spinner.hide();
      },
      (error) => {},
      () => {
        this.checkServiceLocations(serviceLocations);
        this.getTypes();
        this.getStates();
      }
    );
  }

  checkServiceLocations(response: any) {
    if (response.length > 0) {
      for (let i = 1; i <= response.length; i++) {
        this.setFormGroup();
      }
      this.moreRegions.patchValue(response);
    }
  }

  createFormGroup() {
    return this.fb.group({
      state: ['', Validators.required],
      city: ['', Validators.required],
    });
  }

  setFormGroup() {
    this.moreRegions.push(
      this.fb.group({
        state: ['', Validators.required],
        city: ['', Validators.required],
      })
    );
  }

  get moreRegions(): FormArray {
    return this.regionsForm.get('moreRegions') as FormArray;
  }

  get serviceLocations(): FormArray {
    return this.profileForm.get('serviceLocations') as FormArray;
  }

  addOtherRegion(): void {
    if ((this.moreRegions as FormArray).length < 9) {
      (this.moreRegions as FormArray).push(this.createFormGroup());
      this.clicked = true;
    }
  }

  removeRegion(index): void {
    this.moreRegions.removeAt(index);
  }

  getStates() {
    this.spinner.show();
    this.utilsService.getStates().subscribe(
      (response) => {
        this.states = response
          .map((uf) => {
            const state = {
              id: uf.id,
              sigla: uf.sigla,
              nome: uf.nome,
            };
            return state;
          })
          .sort((a, b) => {
            const x = a.nome.toLowerCase();
            const y = b.nome.toLowerCase();
            if (x < y) {
              return -1;
            }
            if (x > y) {
              return 1;
            }
          });
        this.regionState = response;
        this.spinner.hide();
      },
      (error) => {
        console.log(error.error.message);
      },
      () => {
        if (this.regionsForm.get('state').value) {
          this.onStateChange(this.regionsForm.get('state').value);
        }
        for (let i = 0; i <= this.moreRegions.length; i++) {
          if (this.moreRegions.value[i]) {
            this.onStateRegionChange(this.moreRegions.value[i].state, i);
          }
        }
      }
    );
  }

  onStateChange(event) {
    this.stateName = event;
    let idState;
    for (const item of this.states) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityById(idState);
        break;
      }
    }
  }

  onStateRegionChange(event, index) {
    let idState;
    if (event === '') {
      for (const item of this.regionState) {
        if (item.sigla === event) {
          idState = item.id;
          this.getCityRegionById(idState);
          break;
        }
      }
    }
    if (event !== '') {
      this.regionCity[index] = null;
      for (const item of this.regionState) {
        if (item.sigla === event) {
          idState = item.id;
          this.utilsService.getCity(idState).subscribe((response) => {
            this.regionCity[index] = response;
          });
          break;
        }
      }
    }
  }

  getCityRegionById(id) {
    this.utilsService.getCity(id).subscribe((response) => {
      this.regionCity.push(response);
    });
  }

  getCityById(id) {
    this.utilsService.getCity(id).subscribe((response) => {
      this.cities = response;
    });
  }

  onCityChange(event) {
    this.cityName = event;
  }

  sendImg(event): void {
    this.spinner.show();
    this.imgPost = new FormData();

    const reader = new FileReader();
    this.imgRead = true;

    reader.readAsDataURL(event.target.files[0]);
    reader.onload = () => {
      this.imgUrlForm = reader.result as string;
    };
    this.imgPost.append('profile', event.target.files[0]);

    this.userService.updateImage(this.imgPost).subscribe(
      (response) => {
        this.spinner.hide();
        Swal.fire({
          icon: 'success',
          text: 'Imagem de perfil atualizada com sucesso!',
        });
        localStorage.removeItem('image_url');
        localStorage.setItem('image_url', response.data.img_profile);
        this.utilsService.headerImg.next(response.data.img_profile);
      },
      (error) => {
        this.spinner.hide();
      }
    );
  }

  getTypes() {
    this.types = this.profileForm.get('types').value.map(type => type.id).sort();
    if (this.types.length > 0) {
      for (let type of this.types) {
        console.log(type)
        let el = document.getElementById(type) as HTMLInputElement;
        el.checked = true;
      }
    }
  }


  onSubmit() {
    this.isSubmitted = true;
    const regions = [
      {
        state: this.regionsForm.get('state').value,
        city: this.regionsForm.get('city').value,
      },
      ...this.regionsForm.get('moreRegions').value,
    ];
    this.profileForm.get('serviceLocations').patchValue(regions);
    this.profileForm.get('types').patchValue(this.types);
    if (this.profileForm.invalid) {
      return Swal.fire(
        '',
        'Preencha todos os campos corretamente antes de continuar',
        'warning'
      );
    }

    this.spinner.show();
    return this.partnerService
      .updatePartner(this.userId, this.profileForm.value)
      .subscribe(
        (reponse) => {
          this.spinner.hide();
          Swal.fire({
            icon: 'success',
            title: 'Usuário atualizado com sucesso!',
          });
        },
        (error) => {
          this.spinner.hide();
        }
      );
  }

  changeTypeService(event) {
    const index = this.types.indexOf(parseInt(event))
    if (index > -1) {
      this.types.splice(index, 1);
    } else {
      this.types.push(parseInt(event))
    }
    console.log(this.types)
  }
}
