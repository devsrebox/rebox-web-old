import {
  ActivatedRoute,
  Router
} from '@angular/router';
import {
  Component,
  OnInit
} from '@angular/core';
import {
  RequestServicesService
} from 'src/app/core/services/request-services.service';
import {
  NgxSpinnerService
} from 'ngx-spinner';
import {
  FormBuilder,
  Validators
} from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-service-detail',
  templateUrl: './service-detail.component.html',
  styleUrls: ['./service-detail.component.css']
})
export class ServiceDetailComponent implements OnInit {
  service;
  partnerId = localStorage.getItem('c_id');
  serviceId;

  confirmForm = this.fb.group({
    forecast: ['', Validators.required],
    name_partner: ['', Validators.required]
  });

  constructor(
    private requestService: RequestServicesService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.route.params.subscribe(response => {
      this.serviceId = response.id;
    });
  }

  ngOnInit(): void {
    this.getService(this.partnerId, this.serviceId);
  }

  getService(partnerId, serviceId) {
    this.spinner.show();
    this.requestService.getServiceParner(partnerId, serviceId).subscribe(response => {
      console.log(response)
      this.service = response;
      this.spinner.hide();
    })
  }

  recuseService() {
    return Swal.fire({
      text: 'Você tem certeza que deseja cancelar o serviço?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Confirmar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.spinner.show();
        const data = {
          status: 'pendente',
          partner_id: null
        };
        this.requestService.updateService(this.partnerId, this.serviceId, data).subscribe(response => {
          this.spinner.hide();
          Swal.fire(
            '',
            'Recusado com sucesso.',
            'success'
          );
          this.router.navigate(['/companies/requests/'])
        });
      }
    })
  }

  finishService() {
    this.spinner.show();
    const data = {
      status: 'finalizado',
    };
    this.requestService.updateService(this.partnerId, this.serviceId, data).subscribe(response => {
      this.spinner.hide();
      Swal.fire(
        '',
        'Serviço finalizado com sucesso.',
        'success'
      );
      this.router.navigate(['/companies/requests/'])
    })
  }


  onSubmit() {
    this.spinner.show();
    const data = {
      status: 'confirmado',
      partner_id: this.partnerId
    };
    this.requestService.updateService(this.partnerId, this.serviceId, data).subscribe(response => {
      this.spinner.hide();
      Swal.fire(
        '',
        'Serviço aceitado com sucesso.',
        'success'
      );
      this.router.navigate(['/companies/requests/'])
    });
  }
}
