import { NgxSpinnerService } from 'ngx-spinner';
import { UtilsService } from './../../../../core/services/utils.service';
import { ReportService } from './../../../../core/services/report.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-financial-report',
  templateUrl: './financial-report.component.html',
  styleUrls: ['./financial-report.component.css']
})
export class FinancialReportComponent implements OnInit {

  uf;
  city;
  period_of;
  period_until;
  bill;

  stateName;
  cityName
  states;
  cities;

  financialForm = this.fb.group({
    uf: ['', Validators.required],
    city: ['', Validators.required],
    of: ['', Validators.required],
    until: ['', Validators.required],
    bill: ['']
  });

  constructor(
    private fb: FormBuilder,
    private reportService: ReportService,
    private utilsService: UtilsService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.getStates();
  }

  openModal() {
    if (this.financialForm.valid) {
      this.find();
      ($('#ExemploModalCentralizado') as any).modal('show');
    } else {
      Swal.fire('Atenção!', 'Insira os dados corretamente.', 'warning');
    }
  }

  find() {
    this.period_of = this.financialForm.get('of').value;
    this.period_until = this.financialForm.get('until').value;
    this.uf = this.financialForm.get('uf').value;
    this.city = this.financialForm.get('city').value;
    this.bill = this.financialForm.get('bill').value;
  }

  getStates() {
    this.spinner.show();
    this.utilsService.getStates().subscribe(response => {
      this.states = response.map((uf) => {
        const state = {
          id: uf.id,
          sigla: uf.sigla,
          nome: uf.nome
        };
        return state;
      }).sort((a, b) => {
        const x = a.nome.toLowerCase();
        const y = b.nome.toLowerCase();
        if (x < y) {
          return -1;
        }
        if (x > y) {
          return 1;
        }
      });
      this.spinner.hide();
    }, error => {
      this.spinner.hide();

    });
  }

  onStateChange(event) {
    this.stateName = event;
    this.financialForm.get('uf').patchValue(event);
    let idState;
    for (const item of this.states) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityById(idState);
      }
    }
  }

  getCityById(id) {
    this.utilsService.getCity(id).subscribe(response => {
      this.cities = response;
    });
  }

  onCityChange(event) {
    this.cityName = event;
    this.financialForm.get('city').patchValue(event);
  }


}
