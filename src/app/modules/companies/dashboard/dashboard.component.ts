import { UtilsService } from './../../../core/services/utils.service';
import { AuthService } from './../../../core/auth/auth.service';
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  imgUrl;
  isOpened = false;
  showSide = true;
  constructor(
    private authService: AuthService,
    private utilsService: UtilsService
  ) {
    this.utilsService.headerImg.subscribe(response => {
      this.imgUrl = response;
    });
  }

  ngOnInit(): void {
  }
  ngOnDestroy() {
    // this.utilsService.headerImg.unsubscribe();
  }

  menuToggle() {
    document.getElementById('mySidenav').style.width = '200px';
    this.showSide = false;
  }

  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
    this.showSide = true;
  }

  logout() {
    this.authService.logout('/companies');
  }

}
