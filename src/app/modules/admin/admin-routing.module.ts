import { RequestServiceDetailComponent } from './pages/request-service-detail/request-service-detail.component';
import { RequestsServicesComponent } from './pages/requests-services/requests-services.component';
import { OrderSummaryComponent } from './pages/order-summary/order-summary.component';
import { RequestTowComponent } from './pages/request-tow/request-tow.component';
import { OperationalReportComponent } from './pages/operational-report/operational-report.component';
import { FinancialReportComponent } from './pages/financial-report/financial-report.component';
import { CreateRequestComponent } from './pages/create-request/create-request.component';
import { PartnerEditComponent } from './pages/partner-edit/partner-edit.component';
import { AdminGuard } from './../../core/guards/admin.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './pages/signin/signin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CustomersComponent } from './pages/customers/customers.component';
import { PartnersComponent } from './pages/partners/partners.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { RequestsComponent } from './pages/requests/requests.component';
import { CustomerEditComponent } from './pages/customer-edit/customer-edit.component';
import { NewRequestComponent } from './pages/new-request/new-request.component';
import { NewRequestServiceComponent } from './pages/new-request-service/new-request-service.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AdminGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'customers'
      },
      {
        path: 'customers',
        component: CustomersComponent
      },
      {
        path: 'customers/:id',
        component: CustomerEditComponent
      },
      {
        path: 'partners',
        component: PartnersComponent
      },
      {
        path: 'partners/:id',
        component: PartnerEditComponent
      },
      {
        path: 'partner/register',
        component:  PartnerEditComponent,
      },
      {
        path: 'services',
        component: RequestsComponent
      },
      {
        path: 'services/create',
        component: CreateRequestComponent
      },
      {
        path: 'services/:id',
        component: CreateRequestComponent
      },
      {
        path: 'reports',
        component: ReportsComponent
      },
      {
        path: 'reports/financial-report',
        component: FinancialReportComponent
      },
      {
        path: 'reports/operational-report',
        component: OperationalReportComponent
      },
      {
        path: 'requests-services',
        component: RequestsServicesComponent
      },
      {
        path: 'requests-services/:id',
        component: RequestServiceDetailComponent
      },
      {
        path: 'new-request',
        component: NewRequestComponent
      },
      {
        path: 'new-request/request-tow',
        component: RequestTowComponent
      },
      {
        path: 'new-request/request-tow/order-summary',
        component: OrderSummaryComponent
      },
      {
        path: 'new-request/new-request-service',
        component: NewRequestServiceComponent
      }
    ]
  },
  {
    path: 'signin',
    component: SignInComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
