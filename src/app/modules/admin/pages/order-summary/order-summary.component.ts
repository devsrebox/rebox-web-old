import { Router } from '@angular/router';
import { PaymentService } from '../../../../core/services/payment.service';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit, OnChanges } from '@angular/core';
import { RequestService } from 'src/app/core/services/request.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.css']
})
export class OrderSummaryComponent implements OnInit, OnChanges {

  requestResume: any;

  constructor(
    private requestService: RequestService,
    private paymentService: PaymentService,
    private spinner: NgxSpinnerService,
    private location: Location,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getDetails();
  }

  getDetails() {
    this.requestResume = JSON.parse(sessionStorage.getItem('order'));
  }

  back() {
    this.location.back();
  }

  onSubmit() {
    // tslint:disable-next-line: variable-name
    let payment_mode = this.requestResume.payment_mode;
    let reboxId;
    this.spinner.show();
    if (this.requestResume.client.reboxId) {
      return this.paymentService.requestPayment(this.requestResume.client.reboxId, payment_mode).subscribe(response => {
        this.spinner.hide();
        this.router.navigate(['/admin/new-request/request-tow']);
        Swal.fire('', 'Reboque cadastrado com sucesso!', 'success')
        sessionStorage.removeItem('order');
      }, error => {
        this.spinner.hide();
        Swal.fire({
          text: `${error.error.message}`,
          icon: 'error'
        });
      })
    } else {
      let data = this.requestResume.client;
      if (this.requestResume.client.use_plan === true) {
        delete this.requestResume.client.vehicle;
      }
      return this.requestService.request(this.requestResume.client).subscribe(response => {
        reboxId = response.reboxService.id;
        if (response.reboxService.amount === 0) {
         payment_mode.type_payment_mode = 'plano';
         payment_mode.payment_mode = 'presencial';
        }
        this.paymentService.requestPayment(reboxId, payment_mode).subscribe(() => {
          this.spinner.hide();
          this.router.navigate(['/admin/new-request/request-tow']);
          Swal.fire('', 'Reboque cadastrado com sucesso!', 'success')
        }, error => {
          this.spinner.hide();
          Swal.fire({
            text: `${error.error.message}`,
            icon: 'error'
          });
        });
      }, error => {
        this.spinner.hide();
      }, () => {

        sessionStorage.removeItem('order');
      });
    }
  }

  ngOnChanges() {
    if (!this.requestResume) {
      this.getDetails();
    }
  }

}
