import { Router, ActivatedRoute } from '@angular/router';
import { ServicesService } from './../../../../core/services/services.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AdminService } from './../../../../core/services/admin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {

  requests;
  services
  isLoading = false;
  page = 1;
  keyword = '';
  constructor(
    private servicesServices: ServicesService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(response => {
      if (response.page) {
        this.page = response.page;
      }
    })
  }

  ngOnInit(): void {
    this.getServices(this.page, this.keyword);
  }

  getServices(page, keyword) {
    this.spinner.show();
    this.isLoading = true;
    this.servicesServices.getAllServices(page, keyword).subscribe(response => {
      this.spinner.hide();
      this.isLoading = false;
      this.services = response;
    });
  }

  findService(event){
    this.keyword = event;
    if (this.keyword.length >= 2) {
    this.getServices(1, this.keyword);
    }
    if (!this.keyword) {
      this.getServices(1, '');
      this.page = 1;
    }
  }

  goToEditPage(state, city) {
    this.router.navigate(['/admin/services/edit'], {
      queryParams: {
        state,
        city,
        page: this.page
      }
    })
  }

  changePage(event) {
    this.page = event;
    this.getServices(this.page, this.keyword);
  }
}
