import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ReportService } from 'src/app/core/services/report.service';
import { UtilsService } from 'src/app/core/services/utils.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-financial-report',
  templateUrl: './financial-report.component.html',
  styleUrls: ['./financial-report.component.css']
})



export class FinancialReportComponent implements OnInit {

  showInput = false;
  uf;
  city;
  period_of;
  period_until;
  bill;
  isAll;
  partnerId;
  mode;
  all;
  stateName;
  cityName
  states;
  cities = [];

  responsePlans = {
    plan1 : {
      price: 0,
      count: 0,
    },
    plan2 : {
      price: 0,
      count: 0,
    }
  }


  financialForm = this.fb.group({
    partnerId: [''],
    uf: ['', Validators.required],
    city: ['', Validators.required],
    of: ['', Validators.required],
    until: ['', Validators.required],
    bill: ['']
  });

  planForm = this.fb.group({
    mode: [''],
    uf: ['', Validators.required],
    city: ['', Validators.required],
    of: ['', Validators.required],
    until: ['', Validators.required],
    all: ['']
  });

  constructor(
    private fb: FormBuilder,
    private reportService: ReportService,
    private utilsService: UtilsService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.getStates();
  }

  getPlan() {
    this.mode = this.planForm.get('mode').value;
    this.period_of = this.planForm.get('of').value;
    this.period_until = this.planForm.get('until').value;
    this.uf = this.planForm.get('uf').value;
    this.city = this.planForm.get('city').value;
    this.all = this.planForm.get('all').value;
    this.spinner.show();
    this.reportService.plans(this.mode, this.period_of, this.period_until, this.uf, this.city, this.all).subscribe((response: any) => {
      console.log(response)
      this.responsePlans.plan1 = response.plan1;
      this.responsePlans.plan2 = response.plan2;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    })
  }

  openModal() {
    if (this.financialForm.valid) {
      this.find();
      ($('#ExemploModalCentralizado') as any).modal('show');
    }
  }

  changeCheck() {
    const only = document.getElementById('partner-only') as HTMLInputElement;
    if (only.checked) {
      this.showInput = true;
      this.isAll = false;
    } else {
      this.showInput = false;
      this.isAll = true;
    }
  }

  changePlanMode(event) {
    const mode = event.target.value;
    this.planForm.get('mode').patchValue(mode);
  }

  find() {
    this.period_of = this.financialForm.get('of').value;
    this.period_until = this.financialForm.get('until').value;
    this.uf = this.financialForm.get('uf').value;
    this.city = this.financialForm.get('city').value;
    this.bill = this.financialForm.get('bill').value;
    if (this.financialForm.get('partnerId').value) {
      this.partnerId = `partner_id=${this.financialForm.get('partnerId').value}`;
    }

  }

  getStates() {
    this.spinner.show();
    this.utilsService.getStates().subscribe(response => {
      this.states = response.map((uf) => {
        const state = {
          id: uf.id,
          sigla: uf.sigla,
          nome: uf.nome
        };
        return state;
      }).sort((a, b) => {
        const x = a.nome.toLowerCase();
        const y = b.nome.toLowerCase();
        if (x < y) {
          return -1;
        }
        if (x > y) {
          return 1;
        }
      });
      this.spinner.hide();
    }, error => {
      this.spinner.hide();

    }, () => {
      this.getPlan();
    });
  }

  onStateChange(event, index) {
    this.stateName = event;
    if (index === 0) {
      this.financialForm.get('uf').patchValue(event);
    }
    if (index === 1) {
      this.planForm.get('uf').patchValue(event);
    }
    let idState;
    if (event === '') {
      for (const item of this.states) {
        if (item.sigla === event) {
          idState = item.id;
          this.getCityById(idState);
          break;
        }
      }
    }
    if (event !== '') {
      this.cities[index] = null;
      for (const item of this.states) {
        if (item.sigla === event) {
          idState = item.id;
          this.utilsService.getCity(idState).subscribe((response) => {
            this.cities[index] = response;
          });
          break;
        }
      }
    }
  }


  getPlans() {
    this.spinner.show();
    if (this.planForm.valid) {
      this.reportService.viewAdminFinance(
        this.planForm.value.uf,
        this.planForm.value.city,
        this.planForm.value.of,
        this.planForm.value.until,
        this.planForm.value.mode).subscribe((response) => {
          this.spinner.hide()
          console.log(response)
          this.responsePlans = response
        })
    } else {
      this.spinner.hide()
      return Swal.fire('' , 'Preencha todos os campos corretamente antes de continuar', 'warning');
    }

  }

  getCityById(id) {
    this.utilsService.getCity(id).subscribe(response => {
      this.cities.push(response);
    });
  }

  onCityChange(event) {
    this.cityName = event;
    this.financialForm.get('city').patchValue(event);
  }
  onCityChangePlan(event) {
    this.cityName = event;
    this.planForm.get('city').patchValue(event);
  }


}
