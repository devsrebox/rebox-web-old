import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRequestServiceComponent } from './new-request-service.component';

describe('NewRequestServiceComponent', () => {
  let component: NewRequestServiceComponent;
  let fixture: ComponentFixture<NewRequestServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewRequestServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRequestServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
