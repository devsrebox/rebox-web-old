import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestServiceDetailComponent } from './request-service-detail.component';

describe('RequestServiceDetailComponent', () => {
  let component: RequestServiceDetailComponent;
  let fixture: ComponentFixture<RequestServiceDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestServiceDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestServiceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
