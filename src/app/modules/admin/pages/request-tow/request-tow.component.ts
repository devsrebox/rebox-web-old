import { RequestService } from 'src/app/core/services/request.service';
import { UtilsService } from './../../../../core/services/utils.service';
import { Router } from '@angular/router';
import { UserService } from '../../../../core/services/user.service';
import { CarService } from '../../../../core/services/car.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { EmployeeService } from '../../../../core/services/employee.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import Swal from 'sweetalert2';
import { NewRequestService } from 'src/app/core/services/new-request.service';
@Component({
  selector: 'app-request-tow',
  templateUrl: './request-tow.component.html',
  styleUrls: ['./request-tow.component.css']
})
export class RequestTowComponent implements OnInit {

  isSubmitted = false;
  isTow = true;
  isFuel = false;

  towByPlan = false;

  originList: Array<any> = [];
  origin: any;
  destiny: any;

  addressOrigin;
  addressDestiny;
  fullAddressOrigin;
  fullAddressDestiny;

  diagnostic: string;
  diagnosticDescription: string;
  location: string;
  locationDescription: string;

  carBrands;
  carModels;
  amount;

  diagnostics: Array<any> = [
    { id: 1, description: 'Parou de funcionar'},
    { id: 2, description: 'Problema na roda'},
    { id: 3, description: 'Câmbio travado'},
    { id: 4, description: 'Capotado'},
    { id: 5, description: 'Roda furtada'},
    { id: 6, description: 'Nenhuma das opções'}
  ];

  locations: Array<any> = [
    { id: 1, description: 'Via pública'},
    { id: 2, description: 'Ribanceira/Fora da via'},
    { id: 3, description: 'Garagem subsolo'},
    { id: 4, description: 'Garagem nível da rua'}
  ];

  requestTowForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    cel: ['', Validators.required],
    lat1: ['', Validators.required],
    lng1: ['', Validators.required],
    lat2: [''],
    lng2: [''],
    origin: ['', Validators.required],
    destiny: ['', Validators.required],
    removal_location_id: ['', Validators.required],
    status_vehicle_id: ['', Validators.required],
    port_vehicle: ['', Validators.required],
    comments: [''],
    scheduling_date: [''],
    vehicle: this.fb.group({
      brand: ['', Validators.required],
      model: ['', Validators.required],
      board: ['', Validators.required],
      is_armored: ['', Validators.required]
    }),
    reboxId: [null],
    use_plan: [false]
  });

  makeRequestTowForm = this.fb.group({
    payment_mode: ['presencial'],
    type_payment_mode: ['', Validators.required]
  });

  scheduleForm = this.fb.group({
    date: [''],
    time: ['']
  });

  options = {
    componentRestrictions: { country: 'BR' }
  };

  @ViewChild('placesRef') placesRef: any;


  constructor(
    private fb: FormBuilder,
    private employeeService: EmployeeService,
    private spinner: NgxSpinnerService,
    private carService: CarService,
    private requestService: RequestService,
    private router: Router,
    private newRequestService: NewRequestService
  ) { }

  ngOnInit(): void {
    this.getUser();
    this.getCarBrands({term: ''});
    this.getData();
  }

  getUser() {
    this.newRequestService.user.subscribe(response => {
      if (response) {
        this.requestTowForm.patchValue(response);
      }
    })
  }

  getData() {
    const order = JSON.parse(sessionStorage.getItem('order'));
    if(order) {
      this.requestTowForm.patchValue(order.client);
      this.location = order.client.removal_location_id;
      this.diagnostic = order.client.status_vehicle_id;
      this.locationDescription = order.additionalInfo.locationDescription;
      this.diagnosticDescription = order.additionalInfo.diagnosticDescription;
    }
  }

  getCarModels(event) {
    const model = event.term || '';
    const brand = this.requestTowForm.value.vehicle.brand;
    this.carService.getModels(brand, model).subscribe(
      response => {
        this.carModels = response;
      },
      error => {
        Swal.fire('Oops...', 'Houve um erro ao buscar os modelos de veículos', 'error')
      }
    );
  }

  getCarBrands(event) {
    const brand = event.term;
    this.carService.getBrands(brand).subscribe(
      response => {
        this.carBrands = response;
      },
      error => {
        Swal.fire('Oops...', 'Houve um erro ao buscar as marcas de veículos', 'error')
      }
    );
  }

  setPortCar(event) {
    this.requestTowForm.get('port_vehicle').patchValue(event.port_car_id);
  }

  setOrigin(address: Address) {
    this.addressOrigin = `${address.name} - ${address.formatted_address}`;
    this.fullAddressOrigin = `${address.name} - ${address.formatted_address}`;
    this.requestTowForm.get('origin').patchValue(this.fullAddressOrigin);
    document.getElementById('originGps').classList.replace('d-block', 'd-none');
    this.requestTowForm.controls.lat1.patchValue(address.geometry.location.lat().toFixed(10));
    this.requestTowForm.controls.lng1.patchValue(address.geometry.location.lng().toFixed(11));
  }

  // SELECTED ADDRESS DESTINY BY PLACE API
  setDestiny(address: any) {
    this.addressDestiny = `${address.name} - ${address.formatted_address}`;
    this.fullAddressDestiny = `${address.name} - ${address.formatted_address}`;
    this.requestTowForm.get('destiny').patchValue(this.fullAddressDestiny);
    document.getElementById('destinyGps').classList.replace('d-block', 'd-none');
    this.requestTowForm.controls.lat2.patchValue(address.geometry.location.lat().toFixed(10));
    this.requestTowForm.controls.lng2.patchValue(address.geometry.location.lng().toFixed(11));
  }

  // SHOW / HIDE MY ORIGIN LOCATION OPTION
  originFocus(event) {
    event.length > 0 ? document.getElementById('originGps').classList.replace('d-none', 'd-block')
      : document.getElementById('originGps').classList.replace('d-block', 'd-none');
  }

  // SHOW / HIDE MY DESTINY LOCATION OPTION
  destinyFocus(event) {
    event.length > 0 ? document.getElementById('destinyGps').classList.replace('d-none', 'd-block')
    : document.getElementById('destinyGps').classList.replace('d-block', 'd-none');
  }

  checkArmored() {
    const yes = document.getElementById('yes') as HTMLInputElement;
    const no = document.getElementById('no') as HTMLInputElement;
    if (yes.checked) {
      this.requestTowForm.get('vehicle').get('is_armored').patchValue(true);
    }
    if (no.checked) {
      this.requestTowForm.get('vehicle').get('is_armored').patchValue(false);
    }
  }

  calculateValue() {
    let data = this.requestTowForm.value;
    this.requestTowForm.get('status_vehicle_id').patchValue(this.diagnostic);
    this.requestTowForm.get('removal_location_id').patchValue(this.location);
    if (this.requestTowForm.invalid) {
      return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning');
    }
    if(this.towByPlan) {
      delete data.vehicle;
    }
    this.spinner.show();
    return this.employeeService.calculateValue(data).subscribe(response => {
      this.amount = response.reboxService.amount;
      this.requestTowForm.get('reboxId').patchValue(response.reboxService.id);
      if (response.reboxService.amount === 0) {
        this.makeRequestTowForm.get('type_payment_mode').patchValue('plano');
        this.makeRequestTowForm.get('payment_mode').patchValue('presencial');
      }
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      Swal.fire({
        text: `${error.error.message}`,
        icon: 'error'
      })
    });
  }

  changeValue() {
    const is_credit_card = document.getElementById('is_credit_card') as HTMLInputElement;
    const is_debit_card = document.getElementById('is_debit_card') as HTMLInputElement;
    const is_cash = document.getElementById('is_cash') as HTMLInputElement;

    // tslint:disable-next-line: forin
    for (const control in this.makeRequestTowForm.controls) {
      this.makeRequestTowForm.get(control).clearValidators();
    }
    if(is_credit_card.checked) {
      this.makeRequestTowForm.get('type_payment_mode').patchValue('Crédito');
    }
    if(is_debit_card.checked) {
      this.makeRequestTowForm.get('type_payment_mode').patchValue('Débito');
    }
    if (is_cash.checked) {
      this.makeRequestTowForm.get('type_payment_mode').patchValue('Dinheiro');
    }
  }

  schedule() {
    const scheduling_date = `${this.scheduleForm.get('date').value} ${this.scheduleForm.get('time').value}`;
    const reboxId = this.requestTowForm.get('reboxId').value;
    this.requestTowForm.get('scheduling_date').patchValue(scheduling_date);
    if (reboxId) {
      return this.requestService.insertSchedulingDate(reboxId, scheduling_date).subscribe(response => {

        this.onSubmit();
      });
    }

    return this.onSubmit();

  }

  setValidators(isPlan) {
    if (isPlan === true) {
      this.requestTowForm.get('port_vehicle').clearValidators();
      this.requestTowForm.get('vehicle').get('brand').clearValidators();
      this.requestTowForm.get('vehicle').get('model').clearValidators();
      this.requestTowForm.get('vehicle').get('board').clearValidators();
      this.requestTowForm.get('vehicle').get('is_armored').clearValidators();
      this.makeRequestTowForm.get('type_payment_mode').clearValidators();
      this.requestTowForm.get('vehicle').get('brand').updateValueAndValidity();
      this.requestTowForm.get('vehicle').get('model').updateValueAndValidity();
      this.requestTowForm.get('vehicle').get('board').updateValueAndValidity();
      this.requestTowForm.get('vehicle').get('is_armored').updateValueAndValidity();
      this.requestTowForm.get('port_vehicle').updateValueAndValidity();
      this.makeRequestTowForm.get('type_payment_mode').updateValueAndValidity();
    } else {
      this.requestTowForm.get('port_vehicle').setValidators([Validators.required]);
      this.requestTowForm.get('vehicle').get('brand').setValidators([Validators.required]);
      this.requestTowForm.get('vehicle').get('model').setValidators([Validators.required]);
      this.requestTowForm.get('vehicle').get('board').setValidators([Validators.required]);
      this.requestTowForm.get('vehicle').get('is_armored').setValidators([Validators.required]);
      this.makeRequestTowForm.get('type_payment_mode').setValidators([Validators.required]);
      this.requestTowForm.get('vehicle').get('brand').updateValueAndValidity();
      this.requestTowForm.get('vehicle').get('model').updateValueAndValidity();
      this.requestTowForm.get('vehicle').get('board').updateValueAndValidity();
      this.requestTowForm.get('vehicle').get('is_armored').updateValueAndValidity();
      this.requestTowForm.get('port_vehicle').updateValueAndValidity();
      this.makeRequestTowForm.get('type_payment_mode').updateValueAndValidity();
    }
  }

  changeTow(event) {
    if(event.target.value === 'true') {
      this.requestTowForm.get('use_plan').patchValue(true);
      this.towByPlan = true;
      this.setValidators(true);
    } else {
      this.requestTowForm.get('use_plan').patchValue(false);
      this.towByPlan = false;
      this.setValidators(false);
    }

  }

  onSubmit() {
    this.isSubmitted = true;
    this.requestTowForm.get('status_vehicle_id').patchValue(this.diagnostic);
    this.requestTowForm.get('removal_location_id').patchValue(this.location);
    if (this.requestTowForm.invalid || this.makeRequestTowForm.invalid) {
      return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning');
    }
    sessionStorage.setItem('order', JSON.stringify({
      client: this.requestTowForm.value,
      payment_mode: this.makeRequestTowForm.value,
      additionalInfo: {
        locationDescription: this.locationDescription,
        diagnosticDescription: this.diagnosticDescription
      }
    }));
    this.router.navigate(['admin/new-request/request-tow/order-summary']);
  }

}
