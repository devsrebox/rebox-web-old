import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AdminService } from './../../../../core/services/admin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.css']
})
export class PartnersComponent implements OnInit {

  partners;
  page = 1;
  isLoading = false;
  keyword = '';
  constructor(
    private adminService: AdminService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(response => {
      if (response.page) {
        this.page = parseInt(response.page);
      }
    });
  }

  ngOnInit(): void {
    this.getPartners(this.page, this.keyword);
  }

  getPartners(page, keyword) {
    this.spinner.show();
    this.adminService.getPartners(page, keyword).subscribe(response => {
      this.partners = response;
      this.spinner.hide();
    });
  }

  findPartner(event) {
    this.keyword = event;
    if (this.keyword.length >= 3) {
      this.getPartners(1, this.keyword);
    }
    if (!this.keyword) {
      this.getPartners(1, '');
    }
  }

  changePage(event) {
    this.page = event;
    this.getPartners(this.page, this.keyword);
  }

  goPartner(id) {
    this.router.navigate(['/admin/partners', id], {
      queryParams: {
        page: this.page
      }
    })
  }

}
