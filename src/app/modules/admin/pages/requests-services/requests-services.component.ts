import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { RequestServicesService } from './../../../../core/services/request-services.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-requests-services',
  templateUrl: './requests-services.component.html',
  styleUrls: ['./requests-services.component.css']
})
export class RequestsServicesComponent implements OnInit {

  requests;
  page = 1;
  isLoading = false;

  constructor(
    private requestServices: RequestServicesService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(response => {
      if (response.page) {
        this.page = parseInt(response.page);
      }
    })
  }

  ngOnInit(): void {
    this.getAllConsumedPlans();
  }

  getAllConsumedPlans() {
    this.spinner.show();
    this.isLoading = true;
    this.requestServices.getAllRequestsServices().subscribe(response => {
      console.log(response);
      this.requests = response;
      this.isLoading = false;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    })
  }

  findRequests(event) {

  }

  changePage(event) {

  }

  goToService(id) {
    this.router.navigate(['/admin/requests-services', id], {
      queryParams: {
        page: this.page
      }
    })
  }

}
