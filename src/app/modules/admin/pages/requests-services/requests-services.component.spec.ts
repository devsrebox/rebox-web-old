import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestsServicesComponent } from './requests-services.component';

describe('RequestsServicesComponent', () => {
  let component: RequestsServicesComponent;
  let fixture: ComponentFixture<RequestsServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestsServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestsServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
