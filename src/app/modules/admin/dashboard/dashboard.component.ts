import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../../core/auth/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AdminService } from './../../../core/services/admin.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  imgUrl = 'assets/images/user.svg';
  isOpened = false;
  showSide = true;
  user = { id: null, email: null}

  constructor(
    private authService: AuthService,
    private adminService: AdminService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.user.id = localStorage.getItem('id') || sessionStorage.getItem('id');
    this.getUser(this.user.id)
  }

  getUser(id) {
    this.spinner.show();
    this.adminService.getCustomerById(id).subscribe(response => {
      this.user.email = response.email;
    })
    this.spinner.hide();
  }

  menuToggle() {
    document.getElementById('mySidenav').style.width = '200px';
    this.showSide = false;
  }

  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
    this.showSide = true;
  }

  logout() {
    this.authService.logout('/admin/');
  }

}
