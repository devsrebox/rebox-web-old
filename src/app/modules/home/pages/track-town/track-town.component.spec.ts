import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackTownComponent } from './track-town.component';

describe('TrackTownComponent', () => {
  let component: TrackTownComponent;
  let fixture: ComponentFixture<TrackTownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackTownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackTownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
