import { Component, OnInit } from '@angular/core';
import { RequestTownService } from '../../../../core/services/request-town.service';
import { UserService } from '../../../../core/services/user.service';
import Swal from 'sweetalert2';
import { FormBuilder, Validators } from '@angular/forms';
import { CarService } from '../../../../core/services/car.service';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;

@Component({
  selector: 'app-choose-a-car',
  templateUrl: './choose-a-car.component.html',
  styleUrls: ['./choose-a-car.component.css']
})
export class ChooseACarComponent implements OnInit {

  id: any;
  vehicle: any;
  cars: any;

  carBrands: Array<any> = [];
  carModels: Array<any> = [];

  newVehicleForm = this.fb.group({
    brand: [null, Validators.required],
    model: [null, Validators.required],
    board: ['', Validators.required],
    is_armored: ['', Validators.required]
  })

  constructor(
    private requestTownService: RequestTownService,
    private userService: UserService,
    private fb: FormBuilder,
    private carService: CarService,
    private spinner: NgxSpinnerService
  ) {
    this.id = localStorage.getItem('id');
  }

  ngOnInit(): void {
    this.getCars();
    this.getCarBrands({term: ''});
  }

  getCarModels(event) {
    const model = event.term || '';
    const brand = this.newVehicleForm.value.brand;
    this.carService.getModels(brand, model).subscribe(
      response => {
        this.carModels = response;
      },
      error => {
        Swal.fire('Oops...', 'Houve um erro ao buscar os modelos de veículos', 'error')
      }
    )
  }

  getCarBrands(event) {
    const brand = event.term;
    this.carService.getBrands(brand).subscribe(
      response => {
        this.carBrands = response;
      },
      error => {
        Swal.fire('Oops...', 'Houve um erro ao buscar as marcas de veículos', 'error')
      }
    )
  }


  getCars() {
    this.userService.getCars(this.id).subscribe(
      response =>  {
        this.cars = response
        this.vehicle = this.cars[0]
      },
      error => {
        this.cars = [];
      }
    )
  }
  
  onChange(event, car) {
    console.log(event)
    console.log(car)
  }

  onNewVehicleSubmit() {
    if (this.newVehicleForm.invalid) {
      return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning');
    }

    const data = {vehicle: this.newVehicleForm.value};

    this.spinner.show();

    this.userService.appendCar(this.id, data).subscribe(
      response => {
        this.spinner.hide();
        Swal.fire('', 'Veículo adicionado com sucesso', 'success').then(
          result => {
            $('#addVehicle').modal('hide');
            this.cars.push(response);
            this.vehicle = response;
          }
        )
      },
      error => {
        this.spinner.hide();
        Swal.fire('', `${error.error.message}`, 'error')
      }
    )
  }

  onSubmit() {
    if (!this.vehicle) return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning')

    this.requestTownService.onChooseACarSubmit(this.vehicle);
  }

}
