import { NgxSpinnerService } from 'ngx-spinner';
import { RequestService } from './../../../../core/services/request.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.css']
})
export class OrderSummaryComponent implements OnInit {

  requestResume = {
    client: JSON.parse(sessionStorage.getItem('signup')),
    location: JSON.parse(sessionStorage.getItem('location')),
    additionalInfo: JSON.parse(sessionStorage.getItem('additionalInfo'))
  };

  requestInfo;

  param;

  diagnostic;
  location;

  diagnostics: Array<any> = [
    { id: 1, description: 'Parou de funcionar'},
    { id: 2, description: 'Problema na roda'},
    { id: 3, description: 'Câmbio travado'},
    { id: 4, description: 'Capotado'},
    { id: 5, description: 'Roda furtada'},
    { id: 6, description: 'Nenhuma das opções'}
  ];

  locations: Array<any> = [
    { id: 1, description: 'Via pública'},
    { id: 2, description: 'Ribanceira/Fora da via'},
    { id: 3, description: 'Garagem subsolo'},
    { id: 4, description: 'Garagem nível da rua'}
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private request: RequestService,
    private spinner: NgxSpinnerService
  ) {
    this.route.queryParams.subscribe(param => {
      this.param = param;
    });
  }

  ngOnInit(): void {
    if (!this.requestResume.client) {
      this.getRebox(this.param.rebox);
    }
  }

  getRebox(id) {
    this.spinner.show();
    this.request.requestById(id).subscribe(response => {
      console.log(response);
      this.requestInfo = response[0];
      this.setAdditionalInfo(response[0].removal_location_id, response[0].status_vehicle_id);
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    })
  }

  setAdditionalInfo(locationId, diagnosticId) {
    for (let location of this.locations) {
      if (locationId === location.id) {
        this.location = location.description;
      }
    }
    for (let diagnostic of this.diagnostics) {
      if (diagnosticId === diagnostic.id) {
        this.diagnostic = diagnostic.description;
      }
    }
  }

  back() {
    this.location.back();
  }

  onSubmit() {
    this.router.navigate(['/payment'], { queryParams: this.param });
  }

}
