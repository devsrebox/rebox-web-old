import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestTowComponent } from './request-tow.component';

describe('RequestTowComponent', () => {
  let component: RequestTowComponent;
  let fixture: ComponentFixture<RequestTowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestTowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestTowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
