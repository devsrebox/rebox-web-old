import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';

import { AuthService } from '../../../../core/auth/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  showForgotPassword: boolean = false;
  showBackButton: boolean = false;

  isSubmited: boolean = false;

  signinForm = this.fb.group({
    cpf: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(6)]],
    role: ['user']
  });

  constructor(
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  showForgotPasswordComponent() {
    this.showForgotPassword = true;
    this.showBackButton = true;
  }

  showSigninComponent() {
    this.showForgotPassword = false;
    this.showBackButton = false;
  }

  onSubmit() {
    this.isSubmited = true;

    if (this.signinForm.invalid) return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning')

    this.spinner.show();

    this.authService.login(this.signinForm.value);
  }

}
