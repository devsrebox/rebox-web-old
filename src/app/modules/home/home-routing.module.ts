import { PlansComponent } from './../../shared/components/plans/plans.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestTowComponent } from './pages/request-tow/request-tow.component';
import { SigninComponent } from './pages/signin/signin.component';
import { SignupComponent } from './pages/signup/signup.component';
import { AdditionalInfoComponent } from './pages/additional-info/additional-info.component';
import { ChooseACarComponent } from './pages/choose-a-car/choose-a-car.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { TrackTownComponent } from './pages/track-town/track-town.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { OrderSummaryComponent } from './pages/order-summary/order-summary.component';


const routes: Routes = [
  {
    path: '',
    component: RequestTowComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'choose-a-car',
    component: ChooseACarComponent
  },
  {
    path: 'additional-info',
    component: AdditionalInfoComponent
  },
  {
    path: 'order-summary',
    component: OrderSummaryComponent
  },
  {
    path: 'signin',
    component: SigninComponent
  },
  {
    path: 'payment',
    component: PaymentComponent
  },
  {
    path: 'tow-tracking',
    component: TrackTownComponent
  },
  {
    path: 'contact-us',
    component: ContactUsComponent
  },
  {
    path: 'plans',
    component: PlansComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
