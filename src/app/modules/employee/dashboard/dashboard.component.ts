import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/auth/auth.service';
import { UtilsService } from 'src/app/core/services/utils.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  imgUrl;
  isOpened = false;
  showSide = true;
  constructor(
    private authService: AuthService,
    private utilsService: UtilsService
  ) { }

  ngOnInit(): void {
    this.utilsService.headerImg.subscribe(response => {
      this.imgUrl = response;
    })
  }

  menuToggle() {
    document.getElementById('mySidenav').style.width = '200px';
    this.showSide = false;
  }

  closeNav() {
    document.getElementById('mySidenav').style.width = '0';
    this.showSide = true;
  }

  logout() {
    this.authService.logout('/companies');
  }

}
