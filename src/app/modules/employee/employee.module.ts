import { UtilitiesModule } from '../../utilities/utilities.module';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { SharedModule } from '../../shared/shared.module';
import { EmployeeRoutingModule } from './employee-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RequestsComponent } from './pages/requests/requests.component';
import { RequestDetailComponent } from './pages/request-detail/request-detail.component';
import { MyProfileComponent } from './pages/my-profile/my-profile.component';

@NgModule({
  declarations: [
    DashboardComponent,
    RequestsComponent,
    RequestDetailComponent,
    MyProfileComponent
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    SharedModule,
    MatAutocompleteModule,
    GooglePlaceModule,
    UtilitiesModule
  ]
})
export class EmployeeModule { }
