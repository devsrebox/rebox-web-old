import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth/auth.service';
import { UtilsService } from './../../../core/services/utils.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { RequestTownService } from '../../../core/services/request-town.service';

import Swal from 'sweetalert2'
import { UserService } from '../../../core/services/user.service';
import { RequestService } from '../../../core/services/request.service';
import { CarService } from '../../../core/services/car.service';

import * as moment from 'moment';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  param;

  carMaxYear = [];

  states;
  cities;
  stateName;
  cityName;

  userId = localStorage.getItem('id');

  isSubmited: boolean = false;
  userExists: boolean = false;
  carBrands: Array<any> = [];
  carModels: Array<any> = [];

  signUpForm = this.fb.group({
    name: ['', Validators.required],
    cpf: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    port_vehicle: [''],
    password: ['', [Validators.required, Validators.minLength(6)]],
    role: ['user'],
    cel: ['', Validators.required],
    uf: ['', Validators.required],
    city: ['', Validators.required],
    address_zip_code: [''],
    address_neighborhood: [''],
    address_street: [''],
    vehicle: this.fb.group({
      brand: [null, Validators.required],
      model: [null, Validators.required],
      board: ['', Validators.required],
      is_armored: ['', Validators.required],
      year: ['', Validators.required],
      color: ['', Validators.required]
    }),
    // indication_code: ['']
  });


  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private carService: CarService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private router: Router,
    private utilsService: UtilsService
  ) {
    this.route.queryParams.subscribe(response => {
      this.param = response.plan;
    });
  }

  ngOnInit(): void {
    this.getCurrentYear();
    this.getStates();
    this.getCarBrands({term: ''});
    if(sessionStorage.getItem('signup')) {
      this.getSignupData();
    }
    this.getUserDataIfLogged(this.userId);
  }

  getCurrentYear() {
    const year = new Date().getFullYear();
    for (let i = 0; i <= 20; i++ ) {
      this.carMaxYear.push(year-i);
    }
  }

  selectCarYear(event) {
    this.signUpForm.get('vehicle').get('year').patchValue(parseInt(event));
  }

  getUserDataIfLogged(userId) {
    if (this.authService.isLoggedIn()) {
      this.spinner.show();
      this.userService.getUser(userId).subscribe(response => {
        delete response.vehicles;
        this.signUpForm.patchValue(response);
        this.spinner.hide();
      }, error => {
        this.spinner.hide();
      })
    }
  }

  getSignupData() {
    this.signUpForm.patchValue(JSON.parse(sessionStorage.getItem('signup')));
  }

  get vehicle() {
    return this.signUpForm.controls.vehicle as FormGroup;
  }

  checkIfExists(email) {
    const data = {email};
    this.userService.checkIfExists(data).subscribe(
      response => {
        this.userExists = true;
      },
      error => {
        if(error.status == 404) this.userExists = false;
      }
    );
  }

  checkArmored() {
    const yes = document.getElementById('yes') as HTMLInputElement;
    const no = document.getElementById('no') as HTMLInputElement;
    if (yes.checked) {
      this.signUpForm.get('vehicle').get('is_armored').patchValue(true);
    }
    if (no.checked) {
      this.signUpForm.get('vehicle').get('is_armored').patchValue(false);
    }
  }

  getCarModels(event) {
    const model = event.term || '';
    const brand = this.vehicle.value.brand;
    this.carService.getModels(brand, model).subscribe(
      response => {
        this.carModels = response;
        this.carModels.push(
          {
            model: 'MOTO',
            port_car_id: 8
          },
          {
            model: 'TRICÍCLO',
            port_car_id: 2
          },
          {
            model: 'QUADRICÍCLO',
            port_car_id: 3
          }
        );
      },
      error => {
        Swal.fire('Oops...', 'Houve um erro ao buscar os modelos de veículos', 'error')
      }
    )
  }

  getCarBrands(event) {
    const brand = event.term;
    this.carService.getBrands(brand).subscribe(
      response => {
        this.carBrands = response;
      },
      error => {
        Swal.fire('Oops...', 'Houve um erro ao buscar as marcas de veículos', 'error')
      }
    );
  }

  setPortVehicle(event) {
    this.signUpForm.get('port_vehicle').patchValue(event.port_car_id);
  }

  onSubmit() {

    this.isSubmited = true;

    if (this.signUpForm.invalid) {
      return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning');
    }

    if(this.authService.isLoggedIn()) {
      return this.userService.updateUser(this.userId, this.signUpForm.value).subscribe(response => {
        this.router.navigate(['/user/payment'], {
          queryParams: {
            plan: this.param,
            vehicle: response.user.vehicles[0].id
          }
        });
      });
    }

    return this.authService.signupInPlan(this.signUpForm.value,this.param);


    // return this.authService.signup(this.signUpForm.value);

    // if (this.userExists) {
    //   const data = {
    //     email: this.signUpForm.value.email,
    //     password: this.signUpForm.value.password
    //   };

    //   this.userService.authenticate(data).subscribe(
    //     response => {
    //       this.requestTownService.onSignUpSubmit(this.signUpForm.value);
    //     },
    //     error => {
    //       Swal.fire('Oops...', 'Email e/ou senha inválidos.', 'error');
    //     }
    //   )
    // }
    // else {
    //   this.requestTownService.onSignUpSubmit(this.signUpForm.value);
    // }
  }

  getStates() {
    this.spinner.show();
    this.utilsService.getStates().subscribe(
      (response) => {
        this.states = response
          .map((uf) => {
            const state = {
              id: uf.id,
              sigla: uf.sigla,
              nome: uf.nome,
            };
            return state;
          })
          .sort((a, b) => {
            const x = a.nome.toLowerCase();
            const y = b.nome.toLowerCase();
            if (x < y) {
              return -1;
            }
            if (x > y) {
              return 1;
            }
          });
        this.spinner.hide();
      })
  }

  onStateChange(event) {
    this.stateName = event;
    this.signUpForm.get('uf').patchValue(event);
    let idState;
    for (const item of this.states) {
      if (item.sigla === event) {
        idState = item.id;
        this.getCityById(idState);
        break;
      }
    }
  }

  getCityById(id) {
    this.utilsService.getCity(id).subscribe((response) => {
      this.cities = response;
    });
  }

  onCityChange(event) {
    this.cityName = event;
    this.signUpForm.get('city').patchValue(event);
  }

}
