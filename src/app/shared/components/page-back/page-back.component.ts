import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-back',
  templateUrl: './page-back.component.html',
  styleUrls: ['./page-back.component.css']
})
export class PageBackComponent implements OnInit {
  @Input() source;
  constructor(
    private location: Location
  ) { }

  ngOnInit(): void {
  }

  back() {
    this.location.back();
  }

}
