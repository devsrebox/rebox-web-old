import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-image-header',
  templateUrl: './image-header.component.html',
  styleUrls: ['./image-header.component.css']
})
export class ImageHeaderComponent implements OnInit, OnChanges {

  @Input() showBackButton;
  @Output() showSigninEmit = new EventEmitter;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    console.log(this.showBackButton);
  }

  showSignin() {
    this.showSigninEmit.emit(true);
  }

}
