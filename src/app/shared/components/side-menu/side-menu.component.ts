import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../core/auth/auth.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  userImage: string = 'assets/images/user.svg'
  navText: string = 'Entrar';
  isOpen: boolean = false;
  name: string;
  role: string;
  isLoggedIn: boolean;

  constructor(
    private commonService: CommonService,
    private authService: AuthService,
    private router: Router
  ) {
    console.log('ISLOGGED:', this.isLoggedIn)
  }

  ngOnInit(): void {
    this.commonService.openMenu.subscribe( result => this.toggleMenu());
  }

  toggleMenu() {
    this.name = localStorage.getItem('name');
    this.role = localStorage.getItem('role');
    this.isLoggedIn = this.authService.isLoggedIn();

    const sideMenu = document.querySelector('#side-menu');
    const navItems = document.querySelectorAll('.nav-item');

    if (this.isOpen) {
      this.isOpen = false
      sideMenu.classList.remove('open-menu');
      sideMenu.classList.add('closed-menu');
      navItems.forEach( item => item.classList.add('text-nowrap'))
    }
    else {
      this.isOpen = true;
      sideMenu.classList.remove('closed-menu');
      sideMenu.classList.add('open-menu');
      setTimeout(() => {
        navItems.forEach( item => item.classList.remove('text-nowrap'))
      }, 700);
    }
  }

  navigate(path) {
    this.router.navigate([path]);
    this.toggleMenu();
  }

  logout(route) {
    this.authService.logout(route);
    this.toggleMenu();
  }

  signIn() {

    const url = window.location.href;
    if (this.authService.isLoggedIn()) {

      url.indexOf('companies') > -1 ? this.router.navigate(['/companies'])
        : url.indexOf('admin') > -1 ? this.router.navigate(['/admin'])
          : this.router.navigate(['/'])

    }
    else {

      url.indexOf('companies') > -1 ? this.router.navigate(['/companies/signin'])
      : url.indexOf('admin') > -1 ? this.router.navigate(['/admin/signin'])
        : this.router.navigate(['/signin'])
    }

    this.toggleMenu();
  }

}
