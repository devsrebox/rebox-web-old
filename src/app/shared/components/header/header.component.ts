import { UtilsService } from './../../../core/services/utils.service';
import { Component, OnInit, EventEmitter, Input, OnDestroy } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { Location } from '@angular/common';
import { AuthService } from '../../../core/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  @Input() source: string;
  isLoggedIn: boolean = false;
  name: string = '';

  userImg;
  role = localStorage.getItem('role') || sessionStorage.getItem('role');

  constructor(
    private commonService: CommonService,
    private location: Location,
    private authService: AuthService,
    private router: Router,
    private utilsService: UtilsService
  ) {
    this.isLoggedIn = this.authService.isLoggedIn();
    this.name = localStorage.getItem('name');
    this.utilsService.headerImg.subscribe(response => {
      this.userImg = response;
    })
  }

  ngOnInit(): void {}

  toggleMenu() {
    this.commonService.openMenu.emit(true);
  }

  back() {
    this.location.back();
  }

  logout() {
    this.authService.logout('/');
  }

  onClick() {
    const url = window.location.href;
    if (!this.isLoggedIn) {
      url.indexOf('companies') > -1 ? this.router.navigate(['/companies/signin'])
      : url.indexOf('admin') > -1 ? this.router.navigate(['/admin/signin'])
        : this.router.navigate(['/signin'])
    }
  }

  ngOnDestroy() {
    // this.utilsService.headerImg.unsubscribe();
  }

}
