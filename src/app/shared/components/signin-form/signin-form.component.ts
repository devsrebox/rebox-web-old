import { OnChanges } from '@angular/core';
// tslint:disable: no-angle-bracket-type-assertion
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CompaniesService } from '../../../core/services/companies.service';


import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from '../../../core/auth/auth.service';

@Component({
  selector: 'app-signin-form',
  templateUrl: './signin-form.component.html',
  styleUrls: ['./signin-form.component.css']
})
export class SigninFormComponent implements OnInit, OnChanges {

  isSubmited: boolean = false;

  @Input() role: string;
  @Output() showForgotPassword = new EventEmitter();
  @Output() signInFormEmit = new EventEmitter();

  signinForm = this.fb.group({
    cnpj: [''],
    cpf: [''],
    password: ['', [Validators.required, Validators.minLength(6)]],
    role: ['partner']
  });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private CompaniesService: CompaniesService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
  }

  setValidator(role) {
    switch (role) {
      case 'user':
        this.signinForm.get('cnpj').clearValidators();
        this.signinForm.get('cpf').setValidators([Validators.required]);
        this.signinForm.get('cnpj').updateValueAndValidity();
        this.signinForm.get('cpf').updateValueAndValidity();
        break;
      case 'employee':
        this.signinForm.get('cnpj').clearValidators();
        this.signinForm.get('cpf').setValidators([Validators.required]);
        this.signinForm.get('cnpj').updateValueAndValidity();
        this.signinForm.get('cpf').updateValueAndValidity();
        break;
      case 'company' :
        this.signinForm.get('cpf').clearValidators();
        this.signinForm.get('cnpj').setValidators([Validators.required]);
        this.signinForm.get('cpf').updateValueAndValidity();
        this.signinForm.get('cnpj').updateValueAndValidity();
        break;
    }
  }

  onSubmit() {
    this.isSubmited = true;
    if (this.signinForm.invalid) {
      return Swal.fire('', 'Preencha todos os campos corretamente antes de continuar', 'warning');
    }

    this.spinner.show();

    if (this.role === 'user') {
      this.authService.login(this.signinForm.value);
    }
    else if (this.role === 'company') {
      this.authService.partnerLogin(this.signinForm.value);
    }
    else if (this.role === 'employee') {
      this.authService.employeeLogin(this.signinForm.value);
    }
  }

  ngOnChanges() {
    if (this.role) {
      this.setValidator(this.role);
    }
  }

}
