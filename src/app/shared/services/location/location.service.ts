import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { LocationAddress } from "src/app/models/location-address.model";


@Injectable({
  providedIn: "root"
})
export class LocationService {
  constructor() {}

  public fetchAddress(cep): Observable<LocationAddress> {
    return new Observable(x => {
      const request = new XMLHttpRequest();
      request.open("get", `https://viacep.com.br/ws/${cep}/json`, true);
      request.send();
      request.onload = function() {
        const data = JSON.parse(this.response);
        x.next(data);
      };
    });
  }
}
