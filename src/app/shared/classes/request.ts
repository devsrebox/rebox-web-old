import { User } from './user';
import { Vehicles } from './vehicles';

export class Request {
    id?: number;
    partner_id?: number;
    removal_location_id?: number;
    status_payment?: string;
    forecast?: string;
    comments?: string;
    lat1?: string;
    lng1?: string;
    lat2?: string;
    lng2?: string;
    is_present?: boolean;
    scheduling_date: Date
    created_at?: Date;
    updated_at?: Date;
    vehicle_id?: number;
    status_vehicle_id?: number;
    amount?: number;
    from?: string;
    to?: string;
    distance?: string;
    status?: string;
    name_partner?: string;
    partner?: string;
    payments?: Array<Payment> = [new Payment];
    reject_partner: Array<User>;
    timeDescription?: string;

    vehicle?: Vehicles = new Vehicles;

    total?: number;
    perPage?: number;
    page?: number;
    lastPage?: number;
    data?: Array<RequestData> = [];

}

class RequestData {
    id?: number;number;
    partner_id?: number;
    removal_location_id?: number;
    status_payment?: string;
    forecast?: string;
    comments?: string;
    lat1?: string;
    lng1?: string;
    lat2?: string;
    lng2?: string;
    is_present?: boolean;
    created_at?: Date;
    updated_at?: Date;
    vehicle_id?: number;
    status_vehicle_id?: number;
    amount?: number;
    from?: string;
    to?: string;
    distance?: number;
    status?: string;
    name_partner?: string;
    partner?: string;
    payments: Array<Payment> = [new Payment];

    vehicle?: Vehicles = new Vehicles;

}

class Payment {
    id: number;
    rebox_service_id: number;
    description: string;
    created_at: Date;
    updated_at: Date;
    payment_method: string;
    type_payment_mode: string;
}
