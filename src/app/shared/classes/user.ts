export class User {
    id: number;
    email: string;
    password: string;
    cpf: string;
    name: string;
    role: string;
    lat: string;
    lng: string;
    cel: string;
    tel: string;
    created_at: Date
    updated_at: Date
    partner: any
}
