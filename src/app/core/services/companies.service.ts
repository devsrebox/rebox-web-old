import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CompaniesService {


  constructor(
    private http: HttpClient
  ) { }

  getRequests(id, page) {
    return this.http.get<any>(`${environment.API}/rebox/requests/${id}?page=${page}`)
  }

  acceptRequest(partnerID, reboxID, forecast) {
    return this.http.post<any>(`${environment.API}/partners/${partnerID}/rebox/${reboxID}`, {forecast});
  }

  signin(data) {
    return this.http.post<any>(`${environment.API}/auth/authenticate/partner`, data)
  }

  finishRequest(idRebox: any) {
    return this.http.post<any>(`${environment.API}/partners/1/rebox/${idRebox}/status/finish`, {});
  }
}
