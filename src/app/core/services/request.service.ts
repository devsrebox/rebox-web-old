import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  modifyRebox(data, reboxId) {
    return this.http.put<any>(`${environment.API}/rebox/${reboxId}`, data);
  }

  storeToEvaluate(data: { value: any; comments: any; }, reboxId) {
    return this.http.post<any>(`${environment.API}/rebox/${reboxId}/assessments`, data);
  }

  constructor(
    private http: HttpClient
  ) { }

  register(data) {
    return this.http.post<any>(`${environment.API}/auth/register`, data);
  }

  request(data) {
    return this.http.post<any>(`${environment.API}/rebox`, data);
  }

  requestById(id) {
    return this.http.get<any>(`${environment.API}/rebox/${id}`);
  }

  acceptRequest(partnerID, reboxID, data) {
    return this.http.post<any>(`${environment.API}/partners/${partnerID}/rebox/${reboxID}`, data);
  }

  rejectRequest(partnerID, reboxID) {
    return this.http.post<any>(`${environment.API}/partners/${partnerID}/rebox/${reboxID}/reject`, {});
  }

  insertSchedulingDate(id, schedulingDate) {
    return this.http.put<any>(`${environment.API}/rebox/${id}/scheduling-date`, { scheduling_date: schedulingDate });
  }
}
