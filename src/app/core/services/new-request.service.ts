import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewRequestService {

  public user: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  constructor(
    private http: HttpClient
  ) { }

  checkUserPlan(email) {
    return this.http.get(`${environment.API}/users/${email}/plans/isactive`);
  }

  getUserPlans(id) {
    return this.http.get(`${environment.API}/admins/users/${id}/consumed-plans`);
  }

  consumedUserPlan(userId, planId, data) {
    return this.http.post(`${environment.API}/admins/users/${userId}/consumed-plans/${planId}`, data);
  }
}

interface User {
  name: string;
  email: string;
  cel: string;
  id?: number;
}
