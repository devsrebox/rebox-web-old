import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlansService {

  constructor(
    private http: HttpClient
  ) { }

  makePayment(userId, planId, data) {
    return this.http.post<any>(`${environment.API}/users/${userId}/plans/${planId}/payments`, data)
  }

  getAvailableServicesOfPlans(userId) {
    return this.http.get<any>(`${environment.API}/users/${userId}/plans`);
  }

  consumeService(id, data) {
    return this.http.post<any>(`${environment.API}/users/plans/${id}/consume`, data)
  }

  getPlan(userId) {
    return this.http.get<any>(`${environment.API}/users/${userId}/plans/last`);
  }

  getPlanByBoleto(userId) {
    return this.http.get<any>(`${environment.API}/users/${userId}/plans/boleto`);
  }

  updateStatusCodePaymentPlan(userId, paymentId) {
    return this.http.get<any>(`${environment.API}/users/${userId}/plans/payment-plan/${paymentId}/status`);
  }

  cancelPlan(userId, planId) {
    return this.http.get<any>(`${environment.API}/users/${userId}/plans/${planId}/cancel`);
  }

}
