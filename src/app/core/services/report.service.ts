import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(
    private http: HttpClient
  ) { }

  // tslint:disable-next-line: variable-name
  getFinancesReport(type, uf, city, of, until, bill, partner_id = ''): Observable<Blob> {
    return this.http.get(`${environment.API}/partners/reports/finances/${type}?uf=${uf}&city=${city}&initial_date=${of}&final_date=${until}&bill=${bill}`, { responseType: 'blob' });
  }

  getFinancesReportToEmail(type, uf, city, of, until, bill){
    return this.http.get(`${environment.API}/partners/reports/finances/${type}?uf=${uf}&city=${city}&initial_date=${of}&final_date=${until}&bill=${bill}&email=true`);
  }

  getOperationals(type, uf, city, of, until, bill): Observable<Blob>  {
    return this.http.get(`${environment.API}/partners/reports/operationals/${type}?uf=${uf}&city=${city}&initial_date=${of}&final_date=${until}&bill=${bill}`, {responseType: 'blob'});
  }

  getOperationalReportToEmail(type, uf, city, of, until, bill)  {
    return this.http.get(`${environment.API}/partners/reports/operationals/${type}?uf=${uf}&city=${city}&initial_date=${of}&final_date=${until}&bill=${bill}&email=true`);
  }

  getAdminFinancesReportToDownload(type, uf, city, of, until, bill, partnerId = ''): Observable<Blob> {
    return this.http.get(`${environment.API}/reports/finances/${type}?uf=${uf}&city=${city}&initial_date=${of}&final_date=${until}&bill=${bill}&${partnerId}`, {responseType: 'blob'});
  }

  getAdminOperationalsToDownload(type, uf, city, of, until, bill, partnerId = ''): Observable<Blob> {
    return this.http.get(`${environment.API}/reports/operationals/${type}?uf=${uf}&city=${city}&initial_date=${of}&final_date=${until}&bill=${bill}&${partnerId}`, {responseType: 'blob'});
  }

  getAdminFinancesReportToEmail(type, uf, city, of, until, bill, partnerId = '', email) {
    return this.http.get<any>(`${environment.API}/reports/finances/${type}?uf=${uf}&city=${city}&initial_date=${of}&final_date=${until}&bill=${bill}&email=${email}&${partnerId}`);
  }

  getAdminOperationalsReportToEmail(type, uf, city, of, until, bill, partnerId, email) {
    return this.http.get<any>(`${environment.API}/reports/operationals/${type}?uf=${uf}&city=${city}&initial_date=${of}&final_date=${until}&bill=${bill}}&email=${email}&${partnerId}`);
  }

  viewAdminFinance(uf, city, of, until, bill) {
    return this.http.get<any>(`${environment.API}/reports/extracts/plans?uf=${uf}&city=${city}&initial_date=${of}&final_date=${until}&bill=${bill}`);
  }

  plans(mode, of, until, uf, city, all) {
    return this.http.get(`${environment.API}/reports/extract/plans?mode=${mode}&uf=${uf}&city=${city}&initial_date=${of}&final_date=${until}&all=${all}}`);
  }

}
