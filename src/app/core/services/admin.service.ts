import { Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(
    private http: HttpClient
  ) { }

  getCustomers(page, search): Observable<any> {
    return this.http.get<any>(`${environment.API}/admins/users?page=${page}&search=${search}`);
  }

  getCustomerById(id): Observable<any> {
    return this.http.get<any>(`${environment.API}/admins/users/${id}`);
  }

  updateCustomer(id, data) {
    return this.http.put<any>(`${environment.API}/admins/users/${id}`, data);
  }

  removeCustomer(id) {
    return this.http.delete<any>(`${environment.API}/admins/users/${id}`);
  }

  registerPartner(data) {
    return this.http.post<any>(`${environment.API}/admins/partners`, data);
  }


  getPartners(page, keyword) {
    return this.http.get<any>(`${environment.API}/admins/partners?page=${page}&search=${keyword}`);
  }

  getPartnerById(id) {
    return this.http.get<any>(`${environment.API}/admins/partners/${id}`);
  }

  updatePartner(id, data) {
    return this.http.put<any>(`${environment.API}/admins/partners/${id}`, data);
  }

  removePartner(partnerId) {
    return this.http.delete<any>(`${environment.API}/admins/partners/${partnerId}`);
  }

  getRequest(page) {
    return this.http.get<any>(`${environment.API}/admins/rebox?page=${page}`);
  }


  removeRequest(id) {
    return this.http.delete<any>(`${environment.API}/admins/rebox/${id}`);
  }

  changePartnerPassword(id, data) {
    return this.http.post<any>(`${environment.API}/admins/users/${id}/change-password`, data);
  }
}
