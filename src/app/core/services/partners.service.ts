import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PartnersService {

  constructor(
    private http: HttpClient
  ) { }

  updatePartner(id, data) {
    return this.http.put<any>(`${environment.API}/partners/${id}`, data);
  }

  getPartner(id) {
    return this.http.get<any>(`${environment.API}/partners/${id}`);
  }
}
