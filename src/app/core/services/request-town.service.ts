import { Injectable, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../auth/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { RequestService } from './request.service';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class RequestTownService {


  private requestForm = this.fb.group({
    email: [],
    name: [],
    cel: [],
    comments: [],
    removal_location_id: [],
    status_vehicle_id: [],
    port_vehicle: [],
    lat1: [],
    lng1: [],
    lat2: [],
    lng2: [],
    use_plan: [false],
    vehicle: this.fb.group({
      brand: [],
      model: [],
      board: [],
      is_armored: []
    })
  });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private requestService: RequestService
  ) { }

  public onRequestSubmit(event) {
    this.requestForm.patchValue(event);
    const isLogged = this.authService.isLoggedIn();
    const role = localStorage.getItem('role');

    if ( isLogged && role === 'user' ) {
      this.router.navigate(['/choose-a-car'])
    }
    else {
      this.router.navigate(['/signup'])
    }
  }

  public onSignUpSubmit(event) {
    this.requestForm.patchValue(event);
    this.router.navigate(['/additional-info'])
  }

  public onChooseACarSubmit(event) {
    console.log(event)
    this.requestForm.patchValue({vehicle: event});
    console.log(this.requestForm.value)
    this.router.navigate(['/additional-info'])
  }

  public onAdditionalInfoSubmit(event) {
    this.spinner.show();

    this.requestForm.patchValue(event);

    this.requestService.request(this.requestForm.value).subscribe(
      response => {
        this.spinner.hide();
        this.router.navigate(['order-summary'], { queryParams: { rebox: response.reboxService.id } });
      },
      error => {
        console.log(error);
        this.spinner.hide();
        Swal.fire('', error.error.message, 'error')
      }
    )

    return console.log(this.requestForm.value);
  }

  public getOneReboxService(id: number) {
    return new Promise((resolve, reject) => {
      this.spinner.show();
      this.requestService.requestById(id).subscribe(
        response => {
          this.spinner.hide();
          resolve(response);
        },
        error => {
          this.spinner.hide();
          console.log(error)
          Swal.fire('Oops', `${error.error.message}`, 'error')
          reject(error);
        }
      )
    });

  }

  public async toEvaluate(value, comments, reboxId) {
    return new Promise( (resolve, reject) => {
      this.requestService.storeToEvaluate({value, comments}, reboxId).subscribe(
        response => {
          Swal.fire('', 'Avaliação enviada com sucesso', 'success').then( result => this.router.navigate(['/']) )
          resolve('success')
          this.spinner.hide();
        },
        error => {
          Swal.fire('', 'Houve um erro ao enviar sua solicitação, tente novamente mais tarde.', 'error')
          reject('error')
          this.spinner.hide();
        }
      )
    })
  }

  modifyRebox(data: { is_present: number; }, reboxId) {
    this.spinner.show();
    this.requestService.modifyRebox(data, reboxId).subscribe(
      data => {
        Swal.fire('', 'Seu veículo esta sendo rebocado', 'success');
      },
      error => {
        Swal.fire('', 'Houve um erro ao enviar sua solicitação, tente novamente mais tarde.', 'error')
      }
    )
  }

  public setLocation(data) {
    this.requestForm.patchValue(data);
    this.spinner.show();
    this.requestService.request(this.requestForm.value).subscribe(
      response => {
        this.spinner.hide();
        this.router.navigate(['order-summary'], { queryParams: { rebox: response.reboxService.id } });
      },
      error => {
        console.log(error);
        this.spinner.hide();
        Swal.fire('', error.error.message, 'error')
      }
    );
  }


  public onPaymentSuccess(event) {

  }
}
