import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

import * as moment from 'moment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token = localStorage.getItem('token') || sessionStorage.getItem('token');

    if (request.url.indexOf(environment.API) != -1 && token) {
      const cloned = request.clone({headers: request.headers.set('Authorization', 'Bearer ' + token) });

      const expiresAt = moment().add(8, 'hours');

      if (sessionStorage.getItem('token')) {
        sessionStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
      } else {
        localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
      }

      return next.handle(cloned);
    }
    else {
      return next.handle(request);
    }
  }
}
