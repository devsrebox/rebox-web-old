import { AuthService } from './../auth/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const role = JSON.parse(localStorage.getItem('role'));
      const id = JSON.parse(localStorage.getItem('id'));
      const token = JSON.parse(localStorage.getItem('token'));

      const isLoggedIn = this.authService.isLoggedIn();

      if (isLoggedIn && id && token && role === 'user') {
        return true;
      }
      this.router.navigate(['/signin']);
      return false;
  }
}
