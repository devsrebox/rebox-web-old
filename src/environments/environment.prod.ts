export const environment = {
  production: true,
  // API: 'https://test-rebox-api.herokuapp.com/v1',
  // SOCKET_ENDPOINT: 'wss://test-rebox-api.herokuapp.com/',
  // API: 'https://rebox-test-api.herokuapp.com/v1',
  // SOCKET_ENDPOINT: 'wss://rebox-test-api.herokuapp.com/',
  // API: 'http://167.172.221.206/v1',
  // SOCKET_ENDPOINT: 'wss://167.172.221.206/',
  /* BASE URL - CONTROLE INTERNO DA REBOX */
  API: 'https://rebox-api-old.herokuapp.com/v1',
  SOCKET_ENDPOINT: 'wss://rebox-api-old.herokuapp.com/',
  googleAPI: 'https://maps.googleapis.com/maps/api/place/autocomplete/output?json&key=AIzaSyAAY_G1_D1yT5J7mzKqT54Jus3PfzjqoI0'
};
